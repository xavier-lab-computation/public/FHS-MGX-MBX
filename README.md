# FHS-MGX-MBX

Scripts to reproduce the analysis for the FHS metagenomics and metabolomics analysis manuscript.

The data files (metadata and abundance tables) are available at the FHS repository. To access the data a proposal must be submitted at https://www.framinghamheartstudy.org/fhs-for-researchers/research-application/.
