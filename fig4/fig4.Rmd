---
title: "Multiomics analysis - MSP & metabolite"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
library(tidyverse)
library(here)
library(ggessentials)
library(reshape2)
library(patchwork)
```

```{r}
load(here("data/data.Robj")) 
MSPs <- obj$assays$Metagenomics$MSPs
abun <- MSPs$core_msp_30_TPM_median.filtered
anno.tax <- MSPs$metadata_core
pc.msps <- read_csv(here("data/msp_pcoa_correlation.csv"))
```


```{r fig.height=15, fig.width=14}
known <- filter(obj$assays$Metabolomics$metadata, Known) %>% 
  rownames() %>% intersect(colnames(obj$assays$Metabolomics$scaled)) 
metabo.known <- obj$assays$Metabolomics$scaled[, known]
shared.samples <- intersect(rownames(metabo.known), rownames(abun))
abun.fil <- (abun[, colSums(abun>0)/nrow(abun) > 0.02 & colMeans(abun) > 0.05]) # filtered species

msp.metabo <- abun.fil[shared.samples, ]
metabo.known <- metabo.known[shared.samples,]
cor.mat <- cor(metabo.known, msp.metabo, method="spearman")

cor.p <- 
  sapply(1:ncol(metabo.known), function(i)
    apply(msp.metabo, 2, function(x) cor.test(metabo.known[,i], x)$p.value)
  )
cor.fdr <- matrix(p.adjust(cor.p, method = "fdr"), nrow = nrow(cor.p), ncol=ncol(cor.p))
cor.fdr <- t(cor.fdr)

row.ann <- select(obj$assays$Metabolomics$metadata[rownames(cor.mat),], Metabolite) %>% 
  mutate(Metabolite=ifelse(Metabolite=="linoleoyl ethanolamide", "linoleoyl-EA", Metabolite)) %>% 
  mutate(Metabolite=ifelse(Metabolite=="palmitoylethanolamide", "palmitoyl-EA", Metabolite)) %>% 
  mutate(Metabolite=paste0(rownames(.), ":",Metabolite)) 
col.ann <- 
  column_to_rownames(anno.tax, "MSP_ID")[colnames(cor.mat), ] %>% 
  select(MSP, Tax, Lv, species, genus, phylum ) %>% 
  mutate(Tax=ifelse(!is.na(phylum), phylum, Tax)) %>% 
  mutate(Tax=ifelse(!is.na(genus), genus, Tax)) %>% 
  mutate(Tax=ifelse(!is.na(species), species, Tax)) %>% 
  mutate(Tax=paste0(MSP,":",Tax)) 
rownames(cor.mat) <- row.ann$Metabolite
colnames(cor.mat) <- col.ann$Tax

tmp.fil <- abs(cor.mat) > 0.3
cor.mat.fil <- cor.mat[rowSums(tmp.fil)>5, colSums(tmp.fil)>5] 
cor.fdr.fill <- cor.fdr[rowSums(tmp.fil)>5, colSums(tmp.fil)>5]
#cor.fdr.fill <- apply(cor.fdr.fill, 2, function(x) ifelse(x<0.05, "", "N"))
# rownames(cor.mat.fil) %>% write_lines(here("data/msp_correlated_known_matabolite.csv"))

row.color <- read_tsv(here("data/msp_correlated_known_matabolite_anno.csv"), col_names = c("ID", "Class")) %>% 
  data.frame(row.names = 1)

col.color <- select(pc.msps, MSP_ID, PCoA1, PCoA2) %>% 
  mutate(MSP_ID=str_remove(MSP_ID, ".core")) %>%  
  merge(col.ann,by=1) %>% select(Tax, PCoA1, PCoA2) %>% 
  data.frame(row.names = 1)

colors <- geGetColors()[1:15]
names(colors) <- unique(row.color$Class)
colors["other"] <- "grey"
colors <- list(Class=colors)

cor.mat.fil[cor.fdr.fill>=0.5] <- NA

pheatmap::pheatmap(cor.mat.fil,
  annotation_col = col.color,
  annotation_row = row.color,
  annotation_colors = colors,
  na_col = "darkgrey"
  #display_numbers = cor.fdr.fill
  )

```

```{r eval=FALSE, include=FALSE}

pheatmap::pheatmap(cor.mat.fil,
  annotation_col = col.color,
  annotation_row = row.color,
  annotation_colors = colors, 
  width = 14, height = 15,
  filename=here("figures/fig4_correlation_known.pdf"))

googledrive::drive_upload(here("figures/fig4_correlation_known.pdf"), "FHS_Manuscript/raw_figures/fig4_msp_metabolite_association/")
```


```{r}
melt(cor.mat ) %>% 
  write_csv(here("rawdata/msp_known_metabolite_association.csv"))
googledrive::drive_upload(here("rawdata/msp_known_metabolite_association.csv"), "FHS_Manuscript/tables/")

```

## Unknown

```{r}
metabo.unknown <- obj$assays$Metabolomics$scaled[, 
                                !colnames(obj$assays$Metabolomics$scaled) %in% known]
metabo.unknown.raw <- obj$assays$Metabolomics$data[, 
                                !colnames(obj$assays$Metabolomics$scaled) %in% known]
metabo.prev <- apply(metabo.unknown, 2, function(x) sum(x!=min(x))/nrow(metabo.unknown))
metabo.unknown <- metabo.unknown[shared.samples,]
metabo.unknown.raw <- metabo.unknown.raw[shared.samples,]
cor.mat.unknown <- cor(metabo.unknown, msp.metabo, method="spearman")

```

```{r}
melt(cor.mat.unknown) %>% 
  filter(abs(value)>=0.4) %>% 
    write_csv(here("rawdata/msp_unknown_metabolite_association.csv"))
googledrive::drive_upload(here("rawdata/msp_unknown_metabolite_association.csv"), "FHS_Manuscript/tables/")
```
Check association with CVD markers

```{r}
tmp <- melt(cor.mat.unknown) %>% 
  filter(abs(value)>=0.4)
cvd.association <- read_csv(here("rawdata/metabolites_markers_associations.csv"))

head(cvd.association)
head(tmp)
filter(cvd.association, Mb %in% tmp$Var1) %>% 
  filter(Phenotype!="CVDrisk") %>% pull(2) %>% unique() %>% length()
```


```{r}
unknown.anno <- 
  data.frame(compound=rownames(cor.mat.unknown))%>% 
  merge(obj$assays$Metabolomics$annotation, by.x=1, by.y="ID") %>% 
  filter(sub_class!="") %>% 
  count(compound, sub_class) %>% group_by(compound) %>% filter(n>=max(n)) %>%  ## nominal annotation
  mutate(n=n()) %>% filter(n<2) %>% ungroup() %>%  ## filter ambiguous annotation
  select(-n)
```

Positive associations

```{r fig.height=22, fig.width=17.5}
unknown.pos <- melt(cor.mat.unknown) %>% 
  filter(value>0.4) %>% 
  group_by(Var2) %>% 
  mutate(n=n()) %>% 
  filter(n>5) %>% 
  merge(col.ann, by.x=2, by.y=0) %>% 
  mutate(Tax = paste0(Tax, " (", n, ")"))

ann.pos <- merge(unknown.pos, unknown.anno, by.x="Var1", by.y=1) %>% 
  filter(value>0.6) 

library(ggrepel)

ggplot(unknown.pos) + 
  geom_point(aes(x=reorder(Tax, n), y=value), alpha=0.3) + 
  geom_point(data=ann.pos, aes(x=reorder(Tax, n), y=value, color=sub_class),
             size=4,) + 
  geom_text_repel(data=ann.pos, aes(x=reorder(Tax, n), y=value, color=sub_class, label=sub_class), 
                 max.overlaps = 100, size=8) + 
  coord_flip() +
  scale_color_manual(values=geGetColors()[-c(1,2)])+ 
  labs(x=NULL, y="Spearman correlation between abundances of MSPs and metabolites") +
  theme_bw_ge() + 
  guides(color=guide_legend(ncol=2,byrow=TRUE)) +
  theme(
        legend.position = "none",
        legend.title = element_text(angle=90, hjust = 0.5)
    )
```

```{r eval=FALSE, include=FALSE}
file <- "fig4_pos_correlation_unknown.pdf"
ggsave(here("figures", file), width=17.5, height=22)
googledrive::drive_upload(here("figures", file),
                          "FHS_Manuscript/raw_figures/fig4_msp_metabolite_association/")
```

```{r}
filter(unknown.pos, value>0.6) %>% 
  merge(obj$assays$Metabolomics$annotation, by.x="Var1", by.y="ID") %>% View
```

```{r}

which(cor.mat>0.5, arr.ind = T)

colnames(cor.mat.unknown)[which(cor.mat.unknown>0.6, arr.ind = T)[,2] %>% unique]

which.max(cor.mat.unknown[,"msp_120.core"])

```

```{r fig.height=6, fig.width=16}
aux <- function(msp, metabolite=NULL, color="black"){
  if(is.null(metabolite)){
    tmp <- filter(ann.pos, MSP==msp) %>% arrange(desc(value)) %>% slice(1)
  } else{
    tmp <- filter(unknown.pos, MSP==msp, Var1==metabolite)  %>% slice(1)
  }
  tmp.dat <- data.frame(Metabolite=(metabo.unknown[, as.character(tmp$Var1)]), 
             MSP=log10(msp.metabo[, tmp$Var2]+1)) 
  cor <- paste0("Spearman cor=", round(tmp$value,2))
  x.pos <- (max(tmp.dat$MSP)+min(tmp.dat$MSP))/2 + 0.2
  y.pos <- min(tmp.dat$Metabolite)+0.5
  p <- ggplot(tmp.dat,aes(y=Metabolite, x=MSP)) + 
    geom_point(alpha=0.7,  color=color)  + 
    geom_smooth(method="lm", color="black") +
    #scale_y_log10() +
    ggpp::geom_text_npc(data=data.frame(x=1,y=1), size=6,
            npcx=0.5,npcy=0.1, label=cor, inherit.aes = F) +
    #annotate(geom="text", x=x.pos, y=y.pos, label=cor, size=6) +
    labs(y=tmp$Var1, x=str_remove_all(tmp$Tax, "[\\[\\]]") %>% str_replace(":","\n") %>% str_replace("[a-z]+ ", ". "))
  p
}

# aux("msp_016", color=geGetColors()[14])  
p1 <- aux("msp_120", color=geGetColors()[9])
#  scale_x_log10() + scale_y_log10()
p2 <- aux("msp_020", "QI28613_C8")
p3 <- aux("msp_141", "QI29637_C8", color=geGetColors()[4])
  #aux("msp_141", color=geGetColors()[17])
p4 <- aux("msp_021", "QI29995_C8") 
#p5 <- aux("msp_054", "QI30880_C8")

p4+p2+p1+p3 + plot_layout(ncol=4) & (theme_classic_ge())

```

```{r eval=FALSE, include=FALSE}
file <- "fig4_pos_correlation_examples.pdf"
ggsave(here("figures", file), width=16, height=6)
googledrive::drive_upload(here("figures", file), "FHS_Manuscript/raw_figures/fig4_msp_metabolite_association/")
```


Negative associations

```{r fig.height=12, fig.width=17}
unknown.neg <- melt(cor.mat.unknown) %>% 
  filter(value< -0.4) %>% 
  group_by(Var2) %>% 
  mutate(n=n()) %>% 
  filter(n>5) %>% 
  merge(col.ann, by.x=2, by.y=0) %>% 
  mutate(Tax = paste0(Tax, " (", n, ")"))

ann.neg <- merge(unknown.neg, unknown.anno, by.x="Var1", by.y=1) %>% 
  filter(value< -0.51) 

ggplot(unknown.neg) + 
  geom_point(aes(x=reorder(Tax, n), y=value), alpha=0.3) + 
  geom_point(data=ann.neg, aes(x=reorder(Tax, n), y=value, color=sub_class),
             size=4,) + 
  coord_flip() +
  scale_color_manual(values=geGetColors()[-c(1,2)], name="HMDB subclass")+ 
  labs(x=NULL, y="Spearman correlation between abundances of MSPs and metabolites") +
  theme_bw_ge() + 
  guides(color=guide_legend(ncol=2,byrow=TRUE)) +
  theme(
        legend.position = "top",
        legend.title = element_text(angle=90, hjust = 0.5)
    )

```

```{r eval=FALSE, include=FALSE}
file <- "fig4_neg_correlation_unknown.pdf"
ggsave(here("figures", file), width=17.5, height=17)
googledrive::drive_upload(here("figures", file),
                          "FHS_Manuscript/raw_figures/fig4_msp_metabolite_association/")
```

Number of associations for each feature

```{r fig.height=6, fig.width=5}
rbind(melt(cor.mat) %>% 
        filter(abs(value)>0.4) %>% 
        count(Var1) %>% 
        mutate(Known="Known"),
      melt(cor.mat.unknown) %>% 
        filter(abs(value)>0.4) %>% 
        count(Var1) %>% 
        mutate(Known="Unknown")) %>% 
  ggplot(aes(x=n, fill=Known)) + 
  geom_density(bw = 1, alpha=0.5) +
  scale_fill_manual(values=c("cyan", "red"), name=NULL) + 
  labs(y="Density", x="# of strong associations\n(> 0.4 or < -0.4)") + 
  theme_classic_ge()  + 
  theme(legend.position = c(0.5,0.6))
```

```{r eval=FALSE, include=FALSE}
file <- "fig4_association_summary.pdf"
ggsave(here("figures", file), width=5, height=6)
googledrive::drive_upload(here("figures", file), "FHS_Manuscript/raw_figures/fig4_msp_metabolite_association/")
```




## TMA related

```{r}
tmp <- obj$assays$Metabolomics$scaled[,c("QI22241_HILICpos", "QI30398_HILICpos")] 
cor.test(tmp[,1], tmp[,2], method="spearman")
```

```{r fig.height=4, fig.width=20}
rbb.dat <- 
  cor.mat["QI30398_HILICpos:butyrobetaine", (cor.mat["QI30398_HILICpos:butyrobetaine", ])> 0.20 | colnames(cor.mat)=="msp_203:Eggerthella lenta"] %>% 
  data.frame(cor=.) %>% 
  rownames_to_column("name") %>% 
  mutate(id=str_extract(name, ".*:") %>% str_replace(":", ".core")) 

data.frame(msp.metabo[,rbb.dat$id], 
           butyrobetaine=metabo.known[,"QI30398_HILICpos"]) %>% 
  merge(obj$metadata, by=0) %>% 
  select(-1) %>% 
  melt(measure.vars=rbb.dat$id) %>% 
  merge(rbb.dat[,c(1,3)], by.x="variable", by.y="id") %>% 
  mutate(name=str_remove_all(name, "[\\[\\]]") %>% str_replace("[a-z]+ ", ". ")) %>% 
  mutate(presence=ifelse(value>0, "Presence", "Absence")) %>% 
  ggplot(aes(x=presence, y=butyrobetaine)) +
  geom_jitter() +
    geom_boxplot(alpha=0.9) + 
  theme_classic_ge() + 
  ylim(-3,3) +
  ggpubr::stat_compare_means(size=5, label.y.npc = 0.9) +
  facet_wrap(~name, nrow=1, scales="free_x")  +
  labs(x=NULL, y=expression(paste(gamma,'-butyrobetaine')))

```
```{r}
file <- "sup_association_gbb.pdf"
ggsave(here("figures", file), width=20, height=4)
googledrive::drive_upload(here("figures", file), "FHS_Manuscript/raw_figures/fig4_msp_metabolite_association/")
```


## Stool flavonoids
```{r fig.height=5, fig.width=14, warning=FALSE}
names <- c("Phenylacetic acid", 
           "3-Hydroxyphenylacetic acid",
           "3, 5-Dihydroxyphenylacetic acid")
          
compounds <-
  filter(obj$assays$Metabolomics$annotation, name %in% names) %>%
  arrange(abs(ppmDelta)) %>%
  select(ID, annotation=name, ppmDelta) %>%
  mutate(annotation=str_replace_all(annotation, c("Phenylacetic acid"="PA",
                                "3-Hydroxyphenylacetic acid"="HPAA",
                                "3, 5-Dihydroxyphenylacetic acid"="DHPAA"))) 

id <- intersect(compounds$ID, colnames(obj$assays$Metabolomics$scaled))

tmp <- select(data.frame(obj$assays$Metagenomics$MSPs$core_msp_30_TPM_median.filtered), 
       "msp_130.core") %>% 
  merge(select(data.frame(obj$assays$Metabolomics$scaled), all_of(id)), by=0) %>% 
  select(-1) %>% 
  melt(id.vars="msp_130.core") %>% 
  group_by(variable) %>% 
  mutate(cor=cor.test(msp_130.core, value, method="spearman")$estimate,
         p=cor.test(msp_130.core, value, method="spearman")$p.value) %>% 
  merge(compounds, by.x=2, by.y=1) %>% 
  filter(abs(ppmDelta)<1, abs(cor)>0.1) %>% 
  arrange(cor) %>% 
  mutate(variable=str_remove(variable, "_.*")) %>% 
  unite("variable", variable, annotation, sep=":", remove=F)

stats <- distinct(tmp, variable, annotation, c=round(cor,2), p)
stats$sig <- c("***", "***", "***")

ggplot(tmp,aes(y=value,x=msp_130.core+1, col=annotation)) + 
  geom_point(alpha=0.2) + 
  geom_smooth( method = "lm", col="black") + 
  ggpp::geom_text_npc(data=stats, size=6,
            aes(npcx=0.5,npcy=0.1, label=paste0("Spearman cor=", c, ", ", sig)), inherit.aes = F) +
  theme_classic_ge() + 
  #scale_y_log10( ) + 
  scale_x_log10() + 
  scale_color_manual(values=c("red", "blue"), guide=F) + 
  labs(y="Stool phenylacetic acid", x="F. plautii\nRelative abundance (TPM+1)") + 
  facet_wrap(~variable, scales="free")
```

```{r eval=FALSE, include=FALSE}
file <- "fig4_fplautii_paa.pdf"
ggsave(here("figures", file), width=14, height=5)
googledrive::drive_upload(here("figures", file),
                          "FHS_Manuscript/raw_figures/fig4_msp_metabolite_association/")
```

```{r message=FALSE, warning=FALSE, fig.height=5, fig.width=14}
tmp2 <- select(data.frame(obj$assays$Metagenomics$MSPs$core_msp_30_TPM_median.filtered), 
       "msp_405.core") %>% 
  merge(select(data.frame(obj$assays$Metabolomics$scaled), all_of(id)), by=0) %>% 
  select(-1) %>% 
  melt(id.vars="msp_405.core") %>% 
  group_by(variable) %>% 
  mutate(cor=cor.test(msp_405.core, value, method="spearman")$estimate,
         p=cor.test(msp_405.core, value, method="spearman")$p.value) %>% 
  merge(compounds, by.x=2, by.y=1) %>% 
  arrange(cor) %>% 
  mutate(variable=str_remove(variable, "_.*")) %>% 
  unite("variable", variable, annotation, sep=":", remove=F) %>% 
  filter(abs(ppmDelta)<1, variable %in% tmp$variable) 

stats <- distinct(tmp2, variable, annotation, c=round(cor,2), p)
stats$sig <- c("ns", "***", "***")

ggplot(tmp2,aes(y=value,x=msp_405.core+1, col=annotation)) + 
  geom_point(alpha=0.2) + 
  geom_smooth( method = "lm", col="black") + 
  ggpp::geom_text_npc(data=stats, size=6,
            aes(npcx=0.5,npcy=0.1, label=paste0("Spearman cor=", c, ", ", sig)), inherit.aes = F) +
  theme_classic_ge() + 
  #scale_y_log10( ) + 
  scale_x_log10() + 
  scale_color_manual(values=c("red", "blue"), guide=F) + 
  labs(y="Stool phenylacetic acid", x="E. ramulus\nRelative abundance (TPM+1)") + 
  facet_wrap(~variable, scales="free")
```

```{r eval=FALSE, include=FALSE}
file <- "fig4_eramulus_paa.pdf"
ggsave(here("figures", file), width=14, height=5)
googledrive::drive_upload(here("figures", file),
                          "FHS_Manuscript/raw_figures/fig4_msp_metabolite_association/")
```


```{r fig.height=5, fig.width=18, warning=FALSE}
names <- c("3-(2-Hydroxyphenyl)propanoic acid", 
  "3,4-Dihydroxyhydrocinnamic acid",
  "Hydrocinnamic acid")
          
compounds <- 
  filter(obj$assays$Metabolomics$annotation, name %in% names) %>% 
  arrange(abs(ppmDelta)) %>% 
  select(ID, annotation=name, ppmDelta) %>% 
  mutate(annotation=str_replace_all(annotation, c("3-\\(2-Hydroxyphenyl\\)propanoic acid"="HPPA", 
                                "3,4-Dihydroxyhydrocinnamic acid"="DHPPA",
                                "Hydrocinnamic acid"="PPA"), )) %>% 
  filter(!str_detect(ID, "TF6"), !str_detect(ID, "QI15429")) %>% ## TF6 is identical to another compound, QI15429 is similar to QI21021_C18
  rbind(
    data.frame(ID="QI21021_C18",annotation="PPA", ppmDelta=0)
  )

id <- intersect(compounds$ID, colnames(obj$assays$Metabolomics$scaled))

tmp3 <- select(data.frame(obj$assays$Metagenomics$MSPs$core_msp_30_TPM_median.filtered), 
       "msp_130.core") %>% 
  merge(select(data.frame(obj$assays$Metabolomics$scaled), all_of(id)), by=0) %>% 
  select(-1) %>% 
  melt(id.vars="msp_130.core") %>% 
  group_by(variable) %>% 
  mutate(cor=cor.test(msp_130.core, value, method="spearman")$estimate, 
         p=cor.test(msp_130.core, value, method="spearman")$p.value) %>% 
  merge(compounds, by.x=2, by.y=1) %>% 
  filter(abs(ppmDelta)<1, abs(cor)>0.1) %>% 
  arrange(cor) %>% 
  mutate(variable=str_remove(variable, "_.*")) %>% 
  unite("variable", variable, annotation, sep=":", remove=F)

ggplot(tmp3,aes(y=value,x=msp_130.core+1, col=annotation)) + 
  geom_point(alpha=0.2) + 
  geom_smooth( method = "lm", col="black") + 
  ggpp::geom_text_npc(data=distinct(tmp3, variable, annotation, c=round(cor,2)), size=6,
            aes(npcx=0.5,npcy=0.1, label=paste0("Spearman cor=", c,", ***")), inherit.aes = F) +
  theme_classic_ge() + 
  #scale_y_log10( ) + 
  scale_x_log10() + 
  scale_color_manual(values=c("red", "blue", "orange"), guide=F) + 
  labs(y="Stool phenylpropanoic acid", x="F. plautii\nRelative abundance (TPM+1)") + 
  facet_wrap(~variable, scales="free", nrow=1)
```

```{r eval=FALSE, include=FALSE}
file <- "fig4_fplautii_ppa.pdf"
ggsave(here("figures", file), width=18, height=5)
googledrive::drive_upload(here("figures", file),
                          "FHS_Manuscript/raw_figures/fig4_msp_metabolite_association/")
```

```{r message=FALSE, warning=FALSE, fig.height=5, fig.width=18}
tmp4 <- select(data.frame(obj$assays$Metagenomics$MSPs$core_msp_30_TPM_median.filtered), 
       "msp_405.core") %>% 
  merge(select(data.frame(obj$assays$Metabolomics$scaled), all_of(id)), by=0) %>% 
  select(-1) %>% 
  melt(id.vars="msp_405.core") %>% 
  group_by(variable) %>% 
  mutate(cor=cor.test(msp_405.core, value, method="spearman")$estimate,
         p=cor.test(msp_405.core, value, method="spearman")$p.value) %>% 
  merge(compounds, by.x=2, by.y=1) %>% 
  arrange(cor) %>% 
  mutate(variable=str_remove(variable, "_.*")) %>% 
  unite("variable", variable, annotation, sep=":", remove=F) %>% 
  filter(abs(ppmDelta)<1, variable %in% tmp3$variable) 

stats <- distinct(tmp4, variable, annotation, c=round(cor,2), p)
stats$sig <- c("ns", "ns", "ns", "***")

ggplot(tmp4,aes(y=value,x=msp_405.core+1, col=annotation)) + 
  geom_point(alpha=0.2) + 
  geom_smooth( method = "lm", col="black") + 
  ggpp::geom_text_npc(data=stats, size=6,
            aes(npcx=0.5,npcy=0.1, label=paste0("Spearman cor=", c, ", ", sig)), inherit.aes = F) +
  theme_classic_ge() + 
  #scale_y_log10( ) + 
  scale_x_log10() + 
  scale_color_manual(values=c("red", "blue", "orange"), guide=F) + 
  labs(y="Stool phenylpropanoic acid", x="E. ramulus\nRelative abundance (TPM+1)") + 
  facet_wrap(~variable, scales="free", nrow=1)
```
```{r eval=FALSE, include=FALSE}
file <- "fig4_eramulus_ppa.pdf"
ggsave(here("figures", file), width=18, height=5)
googledrive::drive_upload(here("figures", file),
                          "FHS_Manuscript/raw_figures/fig4_msp_metabolite_association/")
```

```{r fig.height=5, fig.width=12} 
selected <- rbind(filter(tmp, abs(cor)>0.2),
      filter(tmp3, abs(cor)>0.2))

ggplot(selected, aes(y=value,x=msp_130.core+1, col=annotation)) + 
  geom_point(alpha=0.2) + 
  geom_smooth( method = "lm", col="black") + 
  ggpp::geom_text_npc(data=distinct(selected, variable, annotation, c=round(cor,2)), size=6,
            aes(npcx=0.5,npcy=0.1, label=paste0("Spearman cor=", c)), inherit.aes = F) +
  theme_classic_ge() + 
  #scale_y_log10( ) + 
  scale_x_log10() + 
  scale_color_manual(values=c("red", "blue", "orange"), guide=F) + 
  labs(y="Stool phenolic acid", x="msp_130 F. plautii") + 
  facet_wrap(~variable, scales="free", nrow=1) + 
  theme(strip.background = element_blank())

```

```{r eval=FALSE, include=FALSE}
file <- "fig4_msp130fplautii_metabolites.pdf"
ggsave(here("figures", file), width=12, height=6)
googledrive::drive_upload(here("figures", file), "FHS_Manuscript/raw_figures/fig4_msp_metabolite_association/")
```

Chalcones and dihydrochalcones

```{r fig.height=7, fig.width=12, message=FALSE, warning=FALSE}
compounds <-
  filter(obj$assays$Metabolomics$annotation, sub_class == "Chalcones and dihydrochalcones", 
         ID %in% colnames(obj$assays$Metabolomics$scaled)) %>% 
  filter(abs(ppmDelta)<1) %>%
  distinct(ID, annotation=sub_class)

id <- unique(compounds$ID)

tmp <- select(data.frame(obj$assays$Metagenomics$MSPs$core_msp_30_TPM_median.filtered), 
       "msp_130.core") %>% 
  merge(select(data.frame(obj$assays$Metabolomics$scaled), all_of(id)), by=0) %>% 
  select(-1) %>% 
  melt(id.vars="msp_130.core") %>% 
  group_by(variable) %>% 
  mutate(cor=cor.test(msp_130.core, value, method="spearman")$estimate,
         p=cor.test(msp_130.core, value, method="spearman")$p.value) %>% 
  merge(compounds, by.x=2, by.y=1) %>% 
  filter(abs(cor)>0.25) %>% 
  arrange(cor) 

stats <- 
  distinct(tmp, variable, annotation, c=round(cor,2), p) %>% 
  mutate(d=c>0) %>% 
  group_by(d) %>% 
  slice_max(abs(c), n=3)

id <- stats$variable

filter(tmp, variable %in% stats$variable) %>% 
ggplot(aes(y=value,x=msp_130.core+1, col=annotation)) + 
  geom_point(alpha=0.2) + 
  geom_smooth( method = "lm", col="black") + 
  ggpp::geom_text_npc(data=stats, size=6,
            aes(npcx=0.5,npcy=0.1, label=paste0("Spearman cor=", c)), inherit.aes = F) +
  theme_classic_ge() + 
  #scale_y_log10( ) + 
  scale_x_log10() + 
  scale_color_manual(values=c("black"), guide=F) + 
  labs(y="Stool chalcones \nand dihydrochalcones", x="msp_130:F. plautii\nRelative abundance (TPM+1)") + 
  facet_wrap(~variable, scales="free", nrow=2) + 
  theme(strip.background = element_blank())
```

```{r eval=FALSE, include=FALSE}
file <- "fig4_fplautii_flavonoid.pdf"
ggsave(here("figures", file), width=12, height=7)
googledrive::drive_upload(here("figures", file),
                          "FHS_Manuscript/raw_figures/fig4_msp_metabolite_association/")
```

```{r fig.height=7, fig.width=12, message=FALSE, warning=FALSE}
tmp5 <- select(data.frame(obj$assays$Metagenomics$MSPs$core_msp_30_TPM_median.filtered), 
       "msp_405.core") %>% 
  merge(select(data.frame(obj$assays$Metabolomics$scaled), all_of(id)), by=0) %>% 
  select(-1) %>% 
  melt(id.vars="msp_405.core") %>% 
  group_by(variable) %>% 
  mutate(cor=cor.test(msp_405.core, value, method="spearman")$estimate,
         p=cor.test(msp_405.core, value, method="spearman")$p.value) %>% 
  merge(compounds, by.x=2, by.y=1) %>% 
  arrange(cor) 

stats <- distinct(tmp5, variable, annotation, c=round(cor,2), p)

ggplot(tmp5, aes(y=value,x=msp_405.core+1, col=annotation)) + 
  geom_point(alpha=0.2) + 
  geom_smooth( method = "lm", col="black") + 
  ggpp::geom_text_npc(data=stats, size=6,
            aes(npcx=0.5,npcy=0.1, label=paste0("Spearman cor=", c)), inherit.aes = F) +
  theme_classic_ge() + 
  #scale_y_log10( ) + 
  scale_x_log10() + 
  scale_color_manual(values=c("black"), guide=F) + 
  labs(y="Stool chalcones \nand dihydrochalcones", x="E. ramulus\nRelative abundance (TPM+1)") + 
  facet_wrap(~variable, scales="free", nrow=2) + 
  theme(strip.background = element_blank())
```

```{r eval=FALSE, include=FALSE}
file <- "fig4_eramulus_flavonoid.pdf"
ggsave(here("figures", file), width=12, height=7)
googledrive::drive_upload(here("figures", file),
                          "FHS_Manuscript/raw_figures/fig4_msp_metabolite_association/")
```
